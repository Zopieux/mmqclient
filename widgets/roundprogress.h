#ifndef ROUNDPROGRESS_H
#define ROUNDPROGRESS_H

#include "../mmqclient.h"
#include <QDebug>
#include <QPainter>
#include <QPainterPath>
#include <QPaintEvent>
#include <QtMath>
#include <QWidget>

class RoundProgress : public QWidget
{
    Q_OBJECT
public:
    explicit RoundProgress(QWidget *parent = 0);
    ~RoundProgress();
    QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent *event);

private:
    static const int BASE_HEIGHT = 19;
    int height, currentRound;
    MMQClient::GameConfig gameConfig;
    QVector<MMQClient::RoundResult> rounds;
    QVector<int> ranks;
    QTimer *timer;
    uint c;

    QColor colorForResult(MMQClient::RoundResult r);

signals:

public slots:
    void setHeight(int height);
    void reset();
    void setGameConfig(const MMQClient::GameConfig conf);
    void setResults(const QVector<MMQClient::RoundResult> rounds);
    void setCurrentRound(int currentRound);
    void setRank(int round, int rank);

private slots:
    void forceRepaint();
};

#endif // ROUNDPROGRESS_H
