#ifndef VALIDITYLABEL_H
#define VALIDITYLABEL_H

#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QPixmap>
#include <QWidget>

class ValidityLabel : public QWidget
{
    Q_OBJECT
public:
    explicit ValidityLabel(QWidget *parent = 0);
    ~ValidityLabel();
    void setLabel(const QString &text);
    void setValid(bool valid);

private:
    bool valid;
    QHBoxLayout *layout;
    QLabel *label, *icon;
    QPixmap *icon_valid, *icon_invalid;

signals:

public slots:

};

#endif // VALIDITYLABEL_H
