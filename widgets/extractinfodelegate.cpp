#include "extractinfodelegate.h"

static const int baseHeight = 30, margin = 3;

ExtractInfoDelegate::ExtractInfoDelegate(QObject *parent) :
    QStyledItemDelegate(parent)
{
}

void ExtractInfoDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option,
                         const QModelIndex &index) const
{
    if (option.state & QStyle::State_Selected)
        painter->fillRect(option.rect, option.palette.highlight());

    painter->save();
    painter->setRenderHint(QPainter::Antialiasing, true);
    painter->setPen(Qt::NoPen);
    painter->setBrush(option.palette.foreground());
    painter->translate(margin, margin);
    QRect r = QRect(option.rect);
    r.setSize(QSize(baseHeight, baseHeight));
    // cover
    QImage img = index.data(Qt::DecorationRole).value<QImage>();
    if(!img.isNull())
        painter->drawImage(r, img.scaled(baseHeight, baseHeight, Qt::KeepAspectRatio, Qt::SmoothTransformation));
    painter->setPen(Qt::black);
    QFont f = QFont(option.font);
    // title
    r = QRect(option.rect);
    r.adjust(baseHeight + margin * 2, 1, -margin, 0);
    r.setHeight(baseHeight / 2);
    f.setBold(true);
    painter->setFont(f);
    painter->drawText(r, index.data(Qt::DisplayRole).toString());
    // artist
    r.moveTop(r.bottom());
    f.setBold(false);
    painter->setFont(f);
    painter->drawText(r, index.data(Qt::UserRole).toString());
    painter->restore();
}

QSize ExtractInfoDelegate::sizeHint(const QStyleOptionViewItem &option,
                             const QModelIndex &index) const
{
    Q_UNUSED(index);
    return QSize(option.rect.width(), baseHeight + margin * 2);
}

