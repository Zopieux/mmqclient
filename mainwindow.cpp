#include "mainwindow.h"
#include "logindialog.h"
#include "mmqclient.h"
#include "widgets/extractinfodelegate.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(MMQClient *client, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    client(client), countdownStart(0), countdownEnd(0),
    hasFirstRound(false), lastChatMessage()
{
    musicPlayer = new QMediaPlayer(this);
    updateTimer = new QTimer(this);
    updateTimer->setInterval(100);
    songBuffer = new QBuffer();

    scoreModel = new PlayerScoreModel(this);
    scoreSortModel = new QSortFilterProxyModel(this);
    scoreSortModel->setSourceModel(scoreModel);
    scoreSortModel->setSortCaseSensitivity(Qt::CaseInsensitive);
    scoreSortModel->setDynamicSortFilter(true);

    historyModel = new SongHistoryModel(this);

    ui->setupUi(this);
    int size = ui->answerInput->geometry().height();
    loadingSpinner = new AnimatedSpinner(QSize(size, size), ui->answerInput),
            ui->fieldOneLabel->setLabel(tr("Title"));
    ui->fieldTwoLabel->setLabel(tr("Artist"));
    ui->apLocalBase->setLabel(tr("Local DB"));
    ui->apEchonest->setLabel(tr("Recognition"));
    ui->scoreboardView->setModel(scoreSortModel);
    ui->scoreboardView->setSortingEnabled(true);
    ui->scoreboardView->sortByColumn(2, Qt::DescendingOrder);
    ui->scoreboardView->horizontalHeader()->setSectionsMovable(false);
    ui->scoreboardView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->scoreboardView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->scoreboardView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->scoreboardView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::Fixed);
    ui->scoreboardView->horizontalHeader()->setSectionResizeMode(4, QHeaderView::Fixed);
    ui->scoreboardView->horizontalHeader()->setSectionResizeMode(5, QHeaderView::ResizeToContents);
    ui->scoreboardView->setColumnWidth(2, 60);
    ui->scoreboardView->setColumnWidth(3, 60);
    ui->scoreboardView->setColumnWidth(4, 60);
    ui->mainSplitter->setStretchFactor(0, 2);
    ui->mainSplitter->setStretchFactor(1, 1);
    ui->boardSplitter->setStretchFactor(0, 1);
    ui->boardSplitter->setStretchFactor(1, 2);

    ui->historyView->setItemDelegate(new ExtractInfoDelegate);
    ui->historyView->setModel(historyModel);

    ui->connectButton->setFocus();

    connect(client, SIGNAL(loginSuccess()), this, SLOT(loginSuccess()));
    connect(client, SIGNAL(loginFailure(MMQClient::LoginError)), this, SLOT(loginFailure(MMQClient::LoginError)));
    connect(client, SIGNAL(gameInit(QString, MMQClient::GameConfig)), this, SLOT(gameInit(QString, MMQClient::GameConfig)));
    connect(client, SIGNAL(gameStateChanged(MMQClient::RoundState)), this, SLOT(gameStateChanged(MMQClient::RoundState)));
    connect(client, SIGNAL(timerChanged(int)), this, SLOT(timerChanged(int)));
    connect(client, SIGNAL(roundChanged(int)), this, SLOT(roundChanged(int)));
    connect(client, SIGNAL(roundChanged(int)), ui->roundProgress, SLOT(setCurrentRound(int)));
    connect(client, SIGNAL(resultsChanged(QVector<MMQClient::RoundResult>)), ui->roundProgress, SLOT(setResults(QVector<MMQClient::RoundResult>)));
    connect(client, SIGNAL(extractReady(QByteArray &)), this, SLOT(extractReady(QByteArray &)));
    connect(client, SIGNAL(playExtract()), this, SLOT(playExtract()));
    connect(client, SIGNAL(guessResult(MMQClient::RoundResult,bool)), this, SLOT(guessResult(MMQClient::RoundResult,bool)));
    connect(client, SIGNAL(chatMessage(MMQClient::ChatMessage)), this, SLOT(chatMessage(MMQClient::ChatMessage)));
    connect(client, SIGNAL(scoreReseted(QList<MMQClient::PlayerScore>&)), scoreModel, SLOT(setScores(QList<MMQClient::PlayerScore>&)));
    connect(client, SIGNAL(scoreReseted(QList<MMQClient::PlayerScore>&)), scoreModel, SLOT(clearRanks()));
    connect(client, SIGNAL(scoreUpdated(int,bool,bool)), scoreModel, SLOT(updateScore(int,bool,bool)));
    connect(client, SIGNAL(playerRank(int,int)), scoreModel, SLOT(setRank(int,int)));
    connect(client, SIGNAL(playerSelfRank(int)), this, SLOT(gotSelfRank(int)));
    connect(client, SIGNAL(newExtractInfo(MMQClient::ExtractInfo)), this, SLOT(newExtractInfo(MMQClient::ExtractInfo)));
    connect(client, SIGNAL(coverReady(int,QImage)), historyModel, SLOT(coverReady(int,QImage)));
    connect(historyModel, SIGNAL(rowsInserted(QModelIndex,int,int)), ui->historyView, SLOT(scrollToBottom()));
    connect(ui->autoplayCheckbox, SIGNAL(stateChanged(int)), this, SLOT(updateAutoplayStatus()));
    connect(ui->premonitionCheckBox, SIGNAL(stateChanged ( int)), this, SLOT(updatePremonitionStatus()));
    connect(ui->muteButton, SIGNAL(toggled(bool)), musicPlayer, SLOT(setMuted(bool)));
    connect(ui->volumeSlider, SIGNAL(sliderMoved(int)), musicPlayer, SLOT(setVolume(int)));
    connect(updateTimer, SIGNAL(timeout()), this, SLOT(updateProgress()));
    connect(ui->connectButton, SIGNAL(clicked()), this, SLOT(connectAction()));
    connect(ui->answerInput, SIGNAL(editingFinished()), this, SLOT(sendGuess()));
    connect(ui->chatInput, SIGNAL(returnPressed()), this, SLOT(sendChat()));

    init();
    ui->statusBar->showMessage(tr("Connect to start playing."));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete musicPlayer;
    delete updateTimer;
    delete songBuffer;
    delete scoreModel;
    delete scoreSortModel;
    delete historyModel;
    delete loadingSpinner;
}

void MainWindow::init()
{
    musicPlayer->stop();
    stopCountDown();
    updateGameStatus();
    ui->connectButton->setText(tr("Connect"));
    ui->fieldOneLabel->setVisible(false);
    ui->fieldTwoLabel->setVisible(false);
    ui->roundProgress->reset();
    ui->usernameLabel->clear();
    ui->answerInput->setReadOnly(true);
    ui->guessResultLabel->setPixmap(QPixmap(":/main/invalid"));
    ui->guessResultLabel->setEnabled(false);
    setUiEnabled(false);
}

void MainWindow::connectAction()
{
    if(client->isConnected()) {
        qDebug() << "logout";
        client->doLogout();
        init();
        ui->statusBar->showMessage(tr("Logged out."));
    } else {
        qDebug() << "connecting";
        LoginDialog dialog(this);
        dialog.setLanguages(client->getLanguages());
        dialog.setModal(true);
        if(dialog.exec() == QDialog::Accepted) {
            ui->connectButton->setEnabled(false);
            ui->statusBar->showMessage(tr("Logging in…"));
            MMQClient::LoginData data = dialog.getLoginData();
            client->doLogin(data);
            updateGameStatus();
        }
    }
}

MMQClient* MainWindow::getClient()
{
    return client;
}

void MainWindow::setUiEnabled(bool enabled)
{
    ui->layGame->setEnabled(enabled);
    ui->layHistory->setEnabled(enabled);
    ui->layScore->setEnabled(enabled);
    ui->layChat->setEnabled(enabled);
}

void MainWindow::loginSuccess()
{
    qDebug() << "login OK";
    ui->connectButton->setText(tr("Logout"));
    ui->connectButton->setEnabled(true);
    ui->statusBar->showMessage(tr("Starting game…"));
    setUiEnabled(true);
    client->startGame();
}

void MainWindow::loginFailure(MMQClient::LoginError error)
{
    qWarning() << "login FAIL because" << error;
    init();
    QString msg = error == MMQClient::InvalidCredentials ? tr("Your credientials are invalid.") : tr("Nickname already in use.");
    QMessageBox::warning(this, tr("Login error"), msg, QMessageBox::Ok);
    ui->statusBar->showMessage(tr("Login failed."), 2);
}

void MainWindow::stopCountDown()
{
    qDebug() << "STOP COUNTDOWN";
    countdownStart = countdownEnd = 0;
    ui->timeBar->setRange(0, 1);
    ui->timeBar->setValue(0);
    ui->timeBar->setFormat(NULL);
}

void MainWindow::startCountDown(int delaysec)
{
    if(delaysec == -1) {
        stopCountDown();
        ui->timeBar->setRange(0, 0); // indeterminate
        ui->timeBar->setFormat("⋅⋅⋅");
    } else {
        countdownStart = QDateTime::currentMSecsSinceEpoch();
        countdownEnd = countdownStart + delaysec * 1000;
        qDebug() << "start COUNTDOWN" << countdownStart << countdownEnd;
        ui->timeBar->setRange(0, delaysec * 1000);
        ui->timeBar->setValue(delaysec * 1000);
        ui->timeBar->setFormat("%p%");
        updateTimer->start();
    }
}

void MainWindow::updateProgress()
{
    if(countdownStart == 0)
        return;
    qint64 v = countdownEnd - QDateTime::currentMSecsSinceEpoch();
    if(v <= 0)
        stopCountDown();
    else
        ui->timeBar->setValue(v);
}

void MainWindow::updateGameStatus()
{
    if(!client->isConnected()) {
        return;
    }
    if(!hasFirstRound && client->getCurrentRound() > 0) {
        hasFirstRound = true;
    }

    MMQClient::RoundState state = client->getCurrentState();
    ui->answerInput->setReadOnly(!(state == MMQClient::Round || state == MMQClient::RoundBreak));
    ui->fieldOneLabel->setEnabled(state == MMQClient::Round);
    ui->fieldTwoLabel->setEnabled(state == MMQClient::Round);

    if(state == MMQClient::Round){
        ui->answerInput->setFocus();
    }
    else if(!(state == MMQClient::Round || state == MMQClient::RoundBreak)) {
        ui->guessResultLabel->setEnabled(false);
        ui->guessResultLabel->setPixmap(QPixmap(":/main/invalid"));
    }

    QString msg;
    QString round = "<em>" + tr("n/a") + "</em>";

    if(state == MMQClient::Idle) {
        msg = tr("Waiting");
    } else if(state == MMQClient::Round) {
        msg = tr("Playing");
    } else if(state == MMQClient::RoundBreak) {
        msg = tr("Round break");
    }
    if(state == MMQClient::Round || state == MMQClient::RoundBreak) {
        int r = client->getCurrentRound();
        if(r > 0)
            round = QString::number(r);
    }
    ui->statusBar->showMessage(tr("Current stage: %1").arg(msg));
    ui->gameStatusLabel->setText(tr("<b>%1</b>").arg(msg));
}

void MainWindow::gameStateChanged(MMQClient::RoundState state)
{
    qDebug() << "GAME STATE CHANGED" << state << "round" << client->getCurrentRound();
    if(!client->isConnected()) {
        return;
    }
    updateGameStatus();
}

void MainWindow::timerChanged(int duration)
{
    stopCountDown();
    startCountDown(duration);
}

void MainWindow::gameInit(QString username, MMQClient::GameConfig conf)
{
    qDebug() << "got conf" << conf.round_count << conf.two_fields;
    ui->scoreboardView->setColumnHidden(4, !conf.two_fields);
    ui->usernameLabel->setText(username);
    ui->roundProgress->reset();
    ui->roundProgress->setGameConfig(conf);
    ui->fieldOneLabel->setVisible(true);
    ui->fieldTwoLabel->setVisible(conf.two_fields);
    ui->answerInput->setReadOnly(true);
    ui->answerInput->setFocus();
    ui->guessResultLabel->setPixmap(QPixmap(":/main/invalid"));
    ui->guessResultLabel->setEnabled(false);
    ui->rankLabelImage->setVisible(false);
    ui->rankLabelText->setVisible(false);
    ui->layChat->setEnabled(client->isConnected() && !client->isAnonymous());
}

void MainWindow::roundChanged(int round)
{
    ui->fieldOneLabel->setValid(false);
    ui->fieldTwoLabel->setValid(false);
    ui->rankLabelImage->setVisible(false);
    ui->rankLabelText->setVisible(false);
    ui->guessResultLabel->setPixmap(QPixmap(":/main/invalid"));
    lastRound = round;
    ui->statusBar->showMessage(tr("Round %1 starts").arg(round));
}

void MainWindow::resultsChanged(QVector<MMQClient::RoundResult> results)
{
    ui->roundProgress->setResults(results);
}

void MainWindow::extractReady(QByteArray &extractToPlay)
{
    musicPlayer->stop();
    delete songBuffer;
    songBuffer = new QBuffer(&extractToPlay);
    songBuffer->open(QIODevice::ReadOnly);
    musicPlayer->setMedia(QMediaContent(), songBuffer);

}

void MainWindow::playExtract(){
    if(client->gameJoined()) {
        musicPlayer->play();
        qDebug() << "      ---------    playing extract!    ---------";
    } else {
        qDebug() << "not playing media because not joined";
    }
    client->resolveExtract();
}

void MainWindow::sendGuess()
{
    if(!client->isConnected())
        return;
    QString guess = ui->answerInput->text();
    if(guess.isEmpty())
        return;
    client->sendGuess(guess);
    ui->answerInput->clear();
    ui->answerInput->setReadOnly(true);
    ui->guessResultLabel->setEnabled(false);
    loadingSpinner->fadeIn();
}

void MainWindow::guessResult(MMQClient::RoundResult res, bool partial)
{
    loadingSpinner->fadeOut();
    if(res != MMQClient::Both) {
        ui->answerInput->setReadOnly(false);
        ui->answerInput->setFocus();
    }

    ui->guessResultLabel->setEnabled(true);
    if(partial) {
        ui->guessResultLabel->setPixmap(QPixmap(":/main/almost"));
    } else {
        ui->guessResultLabel->setPixmap(QPixmap(":/main/invalid"));
    }

    if(res == MMQClient::Empty) {
        ui->fieldOneLabel->setValid(false);
        ui->fieldTwoLabel->setValid(false);
    } else if(res == MMQClient::OnlyOne) {
        ui->fieldOneLabel->setValid(true);
        ui->fieldTwoLabel->setValid(false);
    } else if(res == MMQClient::OnlyTwo) {
        ui->fieldOneLabel->setValid(false);
        ui->fieldTwoLabel->setValid(true);
    } else if(res == MMQClient::Both) {
        ui->fieldOneLabel->setValid(true);
        ui->fieldTwoLabel->setValid(true);
    }
}

void MainWindow::updateAutoplayStatus()
{
    bool enabled = ui->autoplayCheckbox->isChecked();
    ui->apLayout->setEnabled(enabled);
}

void MainWindow::updatePremonitionStatus()
{
    bool val = ui->premonitionCheckBox->isChecked();
    client->setPremonition(val);
}


void MainWindow::chatMessage(MMQClient::ChatMessage msg)
{
    QString body;
    if(msg.type == MMQClient::ChatMessage::Message) {
        if(msg.username == lastChatMessage.username)
            body = msg.message.toHtmlEscaped() + "<br>";
        else
            body = QString("<b>%1</b><br>%2<br>").arg(
                        msg.username.toHtmlEscaped(), msg.message.toHtmlEscaped());
    } else {
        body = QString("<b>%1</b> %2<br>").arg(
                    msg.username.toHtmlEscaped(), msg.type == MMQClient::ChatMessage::Joined ? "joined" : "left");
    }
    lastChatMessage = msg;
    ui->chatView->append(body);
}

void MainWindow::sendChat()
{
    QString msg = ui->chatInput->text();
    ui->chatInput->clear();
    client->sendChat(msg);
}

void MainWindow::newExtractInfo(MMQClient::ExtractInfo info)
{
    historyModel->addExtractInfo(info);
    client->downloadCover(info.item_id);
}

void MainWindow::gotSelfRank(int rank)
{
    QString str;
    if(rank == 1)
        str = tr("First");
    else if(rank == 2)
        str = tr("Second");
    else if(rank == 3)
        str = tr("Third");
    else
        str = tr("%1<sup>th</sup>").arg(rank);
    ui->rankLabelImage->setPixmap(QPixmap(QString(":/main/trophy-%1").arg(rank <= 3 ? rank - 1 : 3)));
    ui->rankLabelText->setText(str);
    ui->rankLabelImage->setVisible(true);
    ui->rankLabelText->setVisible(true);
    ui->roundProgress->setRank(client->getCurrentRound() - 1, rank);
}


