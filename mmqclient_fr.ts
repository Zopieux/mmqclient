<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>LoginDialog</name>
    <message>
        <location filename="logindialog.ui" line="14"/>
        <source>Login to Massive Music Quizz</source>
        <translation>Connexion à Massive Music Quizz</translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="26"/>
        <source>Choose a language, a category and enter your credentials.</source>
        <translation>Choissiez une langue, une catégorie et entrez vos identifiants.</translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="35"/>
        <source>Language</source>
        <translation>Langue</translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="45"/>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="55"/>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="69"/>
        <source>Category</source>
        <translation>Catégorie</translation>
    </message>
    <message>
        <location filename="logindialog.ui" line="79"/>
        <source>Anonymous login</source>
        <translation>Connexion visiteur</translation>
    </message>
</context>
<context>
    <name>MMQClient</name>
    <message>
        <location filename="mmqclient.cpp" line="116"/>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="mmqclient.cpp" line="117"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Massive Music Quizz Client</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="29"/>
        <location filename="mainwindow.cpp" line="93"/>
        <source>Connect</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="107"/>
        <source>Game</source>
        <translation>Jeu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="169"/>
        <source>%v sec</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>Rounds</source>
        <translation>Manche</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="202"/>
        <source>Auto-play</source>
        <translation>Auto-jeu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="253"/>
        <source>History</source>
        <translation>Historique</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="267"/>
        <source>Scoreboard</source>
        <translation>Scores</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="306"/>
        <source>Chat</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="28"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="29"/>
        <source>Artist</source>
        <translation>Artiste</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="30"/>
        <source>Local DB</source>
        <translation>DB locale</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="31"/>
        <source>Recognition</source>
        <translation>Reconnaissance</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="80"/>
        <source>Connect to start playing.</source>
        <translation>Connectez-vous pour jouer.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="112"/>
        <source>Logged out.</source>
        <translation>Déconnecté.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Logging in…</source>
        <translation>Connexion…</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="144"/>
        <source>Logout</source>
        <translation>Déconnexion</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>Starting game…</source>
        <translation>Début du jeu…</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Login error</source>
        <translation>Erreur de connexion</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Your credientials are invalid.</source>
        <translation>Vos identifiants sont invalides.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="156"/>
        <source>Login failed.</source>
        <translation>Connexion échouée.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <source>n/a</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="221"/>
        <source>Waiting</source>
        <translation>Attente</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="223"/>
        <source>Playing</source>
        <translation>En cours</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="225"/>
        <source>Round break</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="232"/>
        <source>Current stage: %1</source>
        <translation>Manche : %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>&lt;b&gt;%1&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Round %1 starts</source>
        <translation>La manche %1 commence</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="383"/>
        <source>First</source>
        <translation>Premier</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="385"/>
        <source>Second</source>
        <translation>Deuxième</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <source>Third</source>
        <translation>Troisième</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="389"/>
        <source>%1&lt;sup&gt;th&lt;/sup&gt;</source>
        <translation>%1&lt;sup&gt;ème&lt;/sup&gt;</translation>
    </message>
    <message>
        <source>%1th</source>
        <translation type="vanished">%1ème</translation>
    </message>
</context>
<context>
    <name>PlayerScoreModel</name>
    <message>
        <location filename="models/playerscoremodel.cpp" line="89"/>
        <source>Team</source>
        <translation>Équipe</translation>
    </message>
    <message>
        <location filename="models/playerscoremodel.cpp" line="91"/>
        <source>Username</source>
        <translation>Pseudonyme</translation>
    </message>
    <message>
        <location filename="models/playerscoremodel.cpp" line="93"/>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <location filename="models/playerscoremodel.cpp" line="95"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="models/playerscoremodel.cpp" line="97"/>
        <source>Artist</source>
        <translation>Artiste</translation>
    </message>
</context>
</TS>
