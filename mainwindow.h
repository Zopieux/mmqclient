#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "mmqclient.h"
#include "widgets/animatedspinner.h"
#include "models/playerscoremodel.h"
#include "models/songhistorymodel.h"
#include <QBuffer>
#include <QFile>
#include <QMainWindow>
#include <QMediaPlayer>
#include <QTimer>
#include <QDialog>
#include <QMessageBox>
#include <QDateTime>
#include <QSortFilterProxyModel>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(MMQClient *client, QWidget *parent = 0);
    ~MainWindow();
    MMQClient* getClient();
    void stopCountDown();
    void startCountDown(int delaysec);

private:
    Ui::MainWindow *ui;
    AnimatedSpinner *loadingSpinner;
    MMQClient *client;
    QTimer *updateTimer;
    QMediaPlayer *musicPlayer;
    PlayerScoreModel *scoreModel;
    SongHistoryModel *historyModel;
    QSortFilterProxyModel *scoreSortModel;
    qint64 countdownStart, countdownEnd;
    QBuffer* songBuffer;
    int lastRound;
    bool hasFirstRound;
    MMQClient::ChatMessage lastChatMessage;
    void init();
    void setUiEnabled(bool);
    void updateGameStatus();

private slots:
    void connectAction();
    void loginSuccess();
    void loginFailure(MMQClient::LoginError error);
    void updateProgress();
    void gameInit(QString username, MMQClient::GameConfig conf);
    void gameStateChanged(MMQClient::RoundState state);
    void resultsChanged(QVector<MMQClient::RoundResult> results);
    void extractReady(QByteArray &extractToPlay);
    void playExtract();
    void timerChanged(int duration);
    void roundChanged(int round);
    void guessResult(MMQClient::RoundResult res, bool partial);
    void sendGuess();
    void updateAutoplayStatus();
    void updatePremonitionStatus();
    void chatMessage(MMQClient::ChatMessage msg);
    void sendChat();
    void newExtractInfo(MMQClient::ExtractInfo info);
    void gotSelfRank(int rank);
};

#endif // MAINWINDOW_H
