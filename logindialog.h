#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include "mmqclient.h"
#include <QDebug>
#include <QDialog>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMediaContent>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QPair>
#include <QPushButton>
#include <QRegExp>
#include <QValidator>

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT


public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();
    void setLanguages(QList<QPair<QString, QString> > lngs);
    MMQClient::LoginData getLoginData() const;

private:
    Ui::LoginDialog *ui;
    MMQClient::LoginData loginData;

signals:

private slots:
    void loadCategories(int index);

public slots:
    void checkValidForm();
};

#endif // LOGINDIALOG_H
