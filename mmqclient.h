#ifndef MMQCLIENT_H
#define MMQCLIENT_H

#include <QCryptographicHash>
#include <QDateTime>
#include <QDir>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMap>
#include <QMediaContent>
#include <QNetworkAccessManager>
#include <QNetworkCookieJar>
#include <QNetworkDiskCache>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QPair>
#include <QPixmap>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlTableModel>
#include <QStandardPaths>
#include <QTemporaryFile>
#include <QThread>
#include <QTimer>
#include <QUrlQuery>
#include <QXmlStreamReader>
#include <QPair>

const int MMQ_LAST_ROUND = 15;

class MMQClient : public QObject
{
    Q_OBJECT

public:
    enum RoundResult {
        None, Unavailable, Empty, OnlyOne, OnlyTwo, Both
    };

    enum RoundState {
        Idle, RoundBreak, Round
    };

    enum LoginError {
        InvalidCredentials, NicknameInUse
    };

    struct GameConfig {
        bool two_fields;
        int round_count;
    };

    struct LoginData {
        QString language, guestUsername, email, password;
        int category;
    };

    struct ExtractInfo {
        int item_id;
        QString fieldone, fieldtwo;
    };

    struct ChatMessage {
        enum Type { Joined, Left, Message } type;
        QString username, message;
        QDateTime timestamp;
    };

    struct PlayerScore {
        QString username, team;
        int pid, uid, score;
        bool has_fieldone, has_fieldtwo, is_self;
    };

    explicit MMQClient(QObject *parent = 0);
    ~MMQClient();
    QList<QPair<QString, QString> > getLanguages() const;
    QList<QPair<int, QString> > getCategories(QString &lng);
    bool isConnected();
    bool isAnonymous();
    bool gameJoined();
    int getCurrentRound();
    QString getLoggedInName();
    RoundState getCurrentState();

private:
    static QSet<QString> stopWords;
    static QMap<QString, QString> numberMap;
    static QMap<QString, QString> initNumberMap();
    static QMap<QString, QSet<QString> > numberLetterMap;
    static QMap<QString, QSet<QString> > initNumberLetterMap();

    static const int
        UPDATE_NORMAL = 1000,
        UPDATE_FAST = 100,
        UPDATE_CHAT = 1500;

    bool connected, playing, joined, firstReq, aliveRetry;
    int currentRound, currentSongid, lastSavedSongId;
    QByteArray lastExtractHash;
    QByteArray extractToPlay;
    QList<QPair<QByteArray,int> > hashList;
    QString connectedUserName;
    int connectedUserId;
    QVector<RoundResult> rounds;
    RoundState currentState;
    GameConfig gameConf;
    LoginData loginData;
    QNetworkAccessManager *netmgr;
    QTimer *pollTimer, *pollChatTimer;
    bool premonition;
    bool emitExtract;
    QMap<QString, QList<QPair<int, QString> > > cachedCategories;
    QMap<QNetworkReply*, QString> netReplyToLangIndex;
    QSqlTableModel *extractModel;
    QDateTime lastChatMessage;
    QSet<int> gotSongId;
    QList<int> fastestPlayerIds;

    qint64 getCurrentTimestamp();
    void getStatsCache();
    void downloadExtract(int uid);
    QByteArray getExtractHash(QByteArray &data);
    void parseScores(const QString &scores);
    bool validHttpReply(const QNetworkReply *reply, int expectedCode);
    QNetworkRequest baseRequest(const QString &path, bool cache_use=false, bool cache_save=false, const QString &lng=QString()) const;

signals:
    void ready();
    void loginSuccess();
    void loginFailure(MMQClient::LoginError error);
    void gameInit(QString username, MMQClient::GameConfig conf);
    void gameStateChanged(MMQClient::RoundState state);
    void timerChanged(int duration);
    void roundChanged(int round);
    void resultsChanged(QVector<MMQClient::RoundResult> rounds);
    void guessResult(MMQClient::RoundResult result, bool partial);
    void extractReady(QByteArray &extractToPlay);
    void playExtract();
    void extractInfoReady(const ExtractInfo);
    void chatMessage(MMQClient::ChatMessage msg);
    void scoreReseted(QList<MMQClient::PlayerScore> &scores);
    void scoreUpdated(int pid, bool, bool);
    void newExtractInfo(MMQClient::ExtractInfo);
    void coverReady(int itemid, QImage cover);
    void playerRank(int pid, int rank);
    void playerSelfRank(int rank);

public slots:
    void sendGuess(const QString &guess);
    void sendChat(const QString &msg);
    void downloadCover(int itemid);
    void doLogin(LoginData data);
    void doLogout(bool send=true);
    void startGame();
    void setPremonition(bool boolean);
    void resolveExtract();

private slots:
    void pollTickRank();
    void pollTickChat();
    void extractDownloaded();
    void netReplyCategories();
    void netReplyLogin();
    void netReplyStartGame();
    void netReplyPollRank();
    void netReplyPollStats();
    void netReplyGuess();
    void netReplyPing();
    void netReplyChat();
    void netReplyCover();

};

#endif // MMQCLIENT_H
