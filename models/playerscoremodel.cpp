#include "playerscoremodel.h"

PlayerScoreModel::PlayerScoreModel(QObject *parent) :
    QAbstractTableModel(parent), _data()
{
}

int PlayerScoreModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return _data.count();
}

int PlayerScoreModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return 6;
}

void PlayerScoreModel::clear(const QModelIndex &parent)
{
    if(_data.count() > 0) {
        beginRemoveRows(QModelIndex(), 0, _data.count()-1);
        _data.clear();
        _ranks.clear();
        _pidToRow.clear();
        endRemoveRows();
        emit dataChanged(parent, parent);
    }
}


void PlayerScoreModel::setScores(QList<MMQClient::PlayerScore> &scorelist)
{
    clear();
    int c = scorelist.count();
    insertRows(0, c, QModelIndex());
    for(int i = 0; i < c; i++) {
        _data.replace(i, scorelist.at(i));
        _pidToRow.insert(scorelist.at(i).pid, i);
    }
    emit dataChanged(index(0, 0), index(c - 1, columnCount() - 1));
}

void PlayerScoreModel::updateScore(int pid, bool has_fieldone, bool has_fieldtwo)
{
    int row = _pidToRow.value(pid, -1);
    if(row == -1) {
        qWarning() << "unknown PID for score change" << pid;
        return;
    }
    _data[row].has_fieldone = has_fieldone;
    _data[row].has_fieldtwo = has_fieldtwo;
    emit dataChanged(index(row, 3), index(row, 4));
}

bool PlayerScoreModel::insertRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    if(count == 0)
        return true;
    beginInsertRows(QModelIndex(), row, row + count - 1);
    for(int i = 0; i < count; i++) {
        _data.insert(row, MMQClient::PlayerScore());
    }
    endInsertRows();
    return true;
}

bool PlayerScoreModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    if(count == 0)
        return true;
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    for(int i = 0; i < count; i++) {
        _pidToRow.remove(_data.at(row).pid);
        _data.removeAt(row);
    }
    endRemoveRows();
    return true;
}

QVariant PlayerScoreModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return QVariant();

    switch(section) {
    case 0:
        return tr("Team");
    case 1:
        return tr("Username");
    case 2:
        return tr("Score");
    case 3:
        return tr("Title");
    case 4:
        return tr("Artist");
    case 5:
        return tr("Rank");
    }

    return QVariant();
}

QVariant PlayerScoreModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();
    const MMQClient::PlayerScore s = _data.at(index.row());
    if(role == Qt::DisplayRole) {
        switch(index.column()) {
        case 0:
            return s.team.isEmpty() ? s.uid >= 0 ? "n/a" : "" : s.team;
        case 1:
            return s.username;
        case 2:
            return s.score + ((s.has_fieldone && 1) || 0) + ((s.has_fieldtwo && 1) || 0);
        case 5:
            if(_ranks.contains(s.pid))
                return _ranks.value(s.pid);
        }
    }
    if(role == Qt::DecorationRole) {
        switch(index.column()) {
        case 3:
            return QPixmap(s.has_fieldone ? ":/main/valid" : ":/main/invalid");
        case 4:
            return QPixmap(s.has_fieldtwo ? ":/main/valid" : ":/main/invalid");
        case 5:
            int r = _ranks.value(s.pid, -1);
            if(r != -1) {
                return QPixmap(QString(":/main/trophy-%1").arg(r <= 3 ? r - 1 : 3));
            }
        }
    }
    if(role == Qt::FontRole) {
        if(s.is_self) {
            QFont f;
            f.setBold(true);
            return f;
        }

    }
    return QVariant();
}

void PlayerScoreModel::setRank(int pid, int rank)
{
    _ranks.insert(pid, rank);
    if(_pidToRow.contains(pid)) {
        int row = _pidToRow.value(pid);
        emit dataChanged(index(row, 5), index(row, 5));
    } else {
        qDebug() << "tried to dataChanged for setRank but dunno this pid";
    }
}

void PlayerScoreModel::clearRanks()
{
    _ranks.clear();
    emit dataChanged(index(0, 0), index(rowCount() - 1, columnCount() - 1));
}
