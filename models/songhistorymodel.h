#ifndef SONGHISTORYMODEL_H
#define SONGHISTORYMODEL_H

#include "../mmqclient.h"
#include <QAbstractListModel>
#include <QImage>


const int MAX_EXTRACT_INFO = 20;


class SongHistoryModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit SongHistoryModel(QObject *parent = 0);

    void clear(const QModelIndex &parent=QModelIndex());
    int rowCount(const QModelIndex &parent=QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    bool insertRows(int row, int count, const QModelIndex &parent);
    bool removeRows(int row, int count, const QModelIndex &parent);

private:
    QList<MMQClient::ExtractInfo> _data;
    QList<QImage> _covers;
    QMap<int,int> _songIdToRow;

signals:

public slots:
    void addExtractInfo(MMQClient::ExtractInfo info);
    void coverReady(int itemid, QImage cover);

};

#endif // SONGHISTORYMODEL_H
