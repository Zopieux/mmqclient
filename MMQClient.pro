#-------------------------------------------------
#
# Project created by QtCreator 2014-06-28T19:25:28
#
#-------------------------------------------------

QT       += core gui network multimedia sql xml

CONFIG   += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MMQClient
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    logindialog.cpp \
    mmqclient.cpp \
    widgets/validitylabel.cpp \
    widgets/roundprogress.cpp \
    widgets/animatedspinner.cpp \
    models/playerscoremodel.cpp \
    models/songhistorymodel.cpp \
    widgets/extractinfodelegate.cpp

HEADERS  += mainwindow.h \
    logindialog.h \
    mmqclient.h \
    widgets/validitylabel.h \
    widgets/roundprogress.h \
    widgets/animatedspinner.h \
    models/playerscoremodel.h \
    models/songhistorymodel.h \
    widgets/extractinfodelegate.h

FORMS    += mainwindow.ui \
    logindialog.ui

RESOURCES += \
    res.qrc

CODECFORTR = UTF-8
TRANSLATIONS = mmqclient_fr.ts
